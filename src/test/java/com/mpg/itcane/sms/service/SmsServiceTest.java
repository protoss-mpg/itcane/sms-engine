package com.mpg.itcane.sms.service;


import com.mpg.itcane.sms.entity.Parameter;
import com.mpg.itcane.sms.entity.ParameterDetail;
import com.mpg.itcane.sms.model.ResponseModel;
import com.mpg.itcane.sms.repository.ParameterDetailRepository;
import com.mpg.itcane.sms.repository.ParameterRepository;
import com.mpg.itcane.sms.util.CommonUtil;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@Slf4j
public class SmsServiceTest {

    @Autowired
    SmsService smsService;

    @Autowired
    ParameterRepository parameterRepository;

    @Autowired
    ParameterDetailRepository parameterDetailRepository;

    @Test
    public  void caseSendSMS_should_be_success(){

        ResponseModel credit = new ResponseModel();
        credit = smsService.checkCreditBalance(credit);
        int start_credit = new Integer(String.valueOf(credit.getData()));

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("to", "0618423814");
        mapParam.put("message", " TEST : caseSendSMSr_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp());
        mapParam.put("template", "simple");
        result = smsService.sendSms(mapParam, result, mapParam.get("template"));


        credit = smsService.checkCreditBalance(credit);
        int end_credit = new Integer(String.valueOf(credit.getData()));

        Assert.assertTrue(result.getSuccess());
        Assert.assertTrue(start_credit > end_credit);


    }

    @Test
    public  void caseSendSMS_WithoutPhoneNumber_should_be_fail(){

        ResponseModel credit = new ResponseModel();
        credit = smsService.checkCreditBalance(credit);
        int start_credit = new Integer(String.valueOf(credit.getData()));

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
//        mapParam.put("to", "0618423814");
        mapParam.put("message", " TEST : caseSendSMSr_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp());
        mapParam.put("template", "simple");
        result = smsService.sendSms(mapParam, result, mapParam.get("template"));


        credit = smsService.checkCreditBalance(credit);
        int end_credit = new Integer(String.valueOf(credit.getData()));

        Assert.assertFalse(result.getSuccess());
        Assert.assertTrue(start_credit == end_credit);


    }

    @Test
    public  void caseSendSMS_NoMessage_should_be_success(){

        ResponseModel credit = new ResponseModel();
        credit = smsService.checkCreditBalance(credit);
        int start_credit = new Integer(String.valueOf(credit.getData()));

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("to", "0618423814");
        mapParam.put("template", "simple");
        result = smsService.sendSms(mapParam, result, mapParam.get("template"));
        log.info("result test [{}]",result);

        credit = smsService.checkCreditBalance(credit);
        int end_credit = new Integer(String.valueOf(credit.getData()));

        Assert.assertTrue(result.getSuccess());
        Assert.assertTrue(start_credit > end_credit);

    }

    @Test
    public  void caseSendSMS_NoTemplate_should_be_fail(){

        ResponseModel credit = new ResponseModel();
        credit = smsService.checkCreditBalance(credit);
        int start_credit = new Integer(String.valueOf(credit.getData()));

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("to", "0618423814");
        mapParam.put("message", " TEST : caseSendSMSr_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp());
        mapParam.put("template", "xxxxxxx");
        result = smsService.sendSms(mapParam, result, mapParam.get("template"));
        log.info("result test [{}]",result);

        credit = smsService.checkCreditBalance(credit);
        int end_credit = new Integer(String.valueOf(credit.getData()));

        Assert.assertFalse(result.getSuccess());
        Assert.assertTrue(start_credit == end_credit);


    }

    @Test
    public  void caseSendSMS_HaveSpecialCharacter_should_be_fail(){

        ResponseModel credit = new ResponseModel();
        credit = smsService.checkCreditBalance(credit);
        int start_credit = new Integer(String.valueOf(credit.getData()));

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("to", "0618423814");
        mapParam.put("message", " AAAA######BBBBB"+CommonUtil.getCurrentDateTimestamp());
        mapParam.put("template", "simple");
        result = smsService.sendSms(mapParam, result, mapParam.get("template"));
        log.info("result test [{}]",result);

        credit = smsService.checkCreditBalance(credit);
        int end_credit = new Integer(String.valueOf(credit.getData()));

        Assert.assertFalse(result.getSuccess());
        Assert.assertEquals(start_credit, end_credit);


    }

    @Test
    public  void caseSendSMS_InvalidNumber_should_be_fail(){

        ResponseModel credit = new ResponseModel();
        credit = smsService.checkCreditBalance(credit);
        int start_credit = new Integer(String.valueOf(credit.getData()));

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("to", "0618423814000123");
        mapParam.put("message", " caseSendSMSHInvalidNumber_should_be_fail "+CommonUtil.getCurrentDateTimestamp());
        mapParam.put("template", "simple");
        result = smsService.sendSms(mapParam, result, mapParam.get("template"));
        log.info("result test [{}]",result);

        credit = smsService.checkCreditBalance(credit);
        int end_credit = new Integer(String.valueOf(credit.getData()));

        Assert.assertFalse(result.getSuccess());
        Assert.assertTrue(start_credit == end_credit);


    }


    /* send by provider*/
    @Test
    public  void caseSendSMSByProvider_should_be_success(){

        ResponseModel credit = new ResponseModel();
        credit = smsService.checkCreditBalance(credit);
        int start_credit = new Integer(String.valueOf(credit.getData()));

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("code", "MKT001");
        mapParam.put("to", "0618423814");
        mapParam.put("message", " TEST : caseSendSMSByProvider_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp());
        mapParam.put("template", "simple");
        result = smsService.sendSmsByProviderCode(mapParam, result, mapParam.get("code"),mapParam.get("template"));
        log.info("result : {}",result);

        credit = smsService.checkCreditBalance(credit);
        int end_credit = new Integer(String.valueOf(credit.getData()));

        Assert.assertTrue(result.getSuccess());
        Assert.assertTrue(start_credit > end_credit);


    }

    @Test
    public  void caseSendSMSByProvider_WithoutPhoneNumber_should_be_fail(){

        ResponseModel credit = new ResponseModel();
        credit = smsService.checkCreditBalance(credit);
        int start_credit = new Integer(String.valueOf(credit.getData()));

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("code", "MKT001");
        mapParam.put("message", " TEST : caseSendSMSByProvider_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp());
        mapParam.put("template", "simple");
        result = smsService.sendSmsByProviderCode(mapParam, result,mapParam.get("code"), mapParam.get("template"));
        log.info("result test caseSendSMSByProviderWithoutPhoneNumber_should_be_success [{}]",result);

        credit = smsService.checkCreditBalance(credit);
        int end_credit = new Integer(String.valueOf(credit.getData()));

        Assert.assertFalse(result.getSuccess());
        Assert.assertTrue(start_credit == end_credit);

    }

    @Test
    public  void caseSendSMSByProvider_NoTemplate_should_be_fail(){

        ResponseModel credit = new ResponseModel();
        credit = smsService.checkCreditBalance(credit);
        int start_credit = new Integer(String.valueOf(credit.getData()));

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("code", "MKT001");
        mapParam.put("to", "0618423814");
        mapParam.put("message", " TEST : caseSendSMSr_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp());
        mapParam.put("template", "xxxxxxx");
        result = smsService.sendSmsByProviderCode(mapParam, result, mapParam.get("code"), mapParam.get("template"));
        log.info("result test [{}]",result);

        credit = smsService.checkCreditBalance(credit);
        int end_credit = new Integer(String.valueOf(credit.getData()));

        Assert.assertFalse(result.getSuccess());
        Assert.assertTrue(start_credit == end_credit);

    }

    @Test
    public  void caseSendSMSByProvider_NoProvider_should_be_fail(){

        ResponseModel credit = new ResponseModel();
        credit = smsService.checkCreditBalance(credit);
        int start_credit = new Integer(String.valueOf(credit.getData()));

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("code", "xxxxx12345");
        mapParam.put("to", "0618423814");
        mapParam.put("message", " TEST : caseSendSMSByProvider_NoProvider_should_be_fail Send Date : "+CommonUtil.getCurrentDateTimestamp());
        mapParam.put("template", "simple");
        result = smsService.sendSmsByProviderCode(mapParam, result, mapParam.get("code"), mapParam.get("template"));
        log.info("result test [{}]",result);

        credit = smsService.checkCreditBalance(credit);
        int end_credit = new Integer(String.valueOf(credit.getData()));

        Assert.assertFalse(result.getSuccess());
        Assert.assertTrue(start_credit == end_credit);

    }

    @Test
    public  void caseSendSMSByProvider_NoMessage_should_be_success(){

        ResponseModel credit = new ResponseModel();
        credit = smsService.checkCreditBalance(credit);
        int start_credit = new Integer(String.valueOf(credit.getData()));

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("code", "MKT001");
        mapParam.put("to", "0618423814");
        mapParam.put("template", "simple");
        result = smsService.sendSmsByProviderCode(mapParam, result, mapParam.get("code"), mapParam.get("template"));
        log.info("result test [{}]",result);

        credit = smsService.checkCreditBalance(credit);
        int end_credit = new Integer(String.valueOf(credit.getData()));

        Assert.assertTrue(result.getSuccess());
        Assert.assertTrue(start_credit > end_credit);

    }

    @Test
    public  void caseSendSMSByProvider_InvalidMobileNumber_should_be_fail(){

        ResponseModel credit = new ResponseModel();
        credit = smsService.checkCreditBalance(credit);
        int start_credit = new Integer(String.valueOf(credit.getData()));

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("code", "MKT001");
        mapParam.put("to", "0618423814999");
        mapParam.put("message", " TEST : caseSendSMSByProvider_InvalidMobileNumber_should_be_fail Send Date : "+CommonUtil.getCurrentDateTimestamp());
        mapParam.put("template", "simple");
        result = smsService.sendSmsByProviderCode(mapParam, result, mapParam.get("code"), mapParam.get("template"));
        log.info("result test [{}]",result);

        credit = smsService.checkCreditBalance(credit);
        int end_credit = new Integer(String.valueOf(credit.getData()));

        Assert.assertFalse(result.getSuccess());
        Assert.assertTrue(start_credit == end_credit);

    }

    @Test
    public  void caseSendSMSByProvider_InvalidSenderName_should_be_fail(){

        ResponseModel credit = new ResponseModel();
        credit = smsService.checkCreditBalance(credit);
        int start_credit = new Integer(String.valueOf(credit.getData()));

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("code", "MKT001");
        mapParam.put("to", "061842");
        mapParam.put("message", " TEST : caseSendSMSByProvider_InvalidMobileNumber_should_be_fail Send Date : "+CommonUtil.getCurrentDateTimestamp());
        mapParam.put("template", "simple");
        result = smsService.sendSmsByProviderCode(mapParam, result, mapParam.get("code"), mapParam.get("template"));
        log.info("result test [{}]",result);

        credit = smsService.checkCreditBalance(credit);
        int end_credit = new Integer(String.valueOf(credit.getData()));

        Assert.assertFalse(result.getSuccess());
        Assert.assertTrue(start_credit == end_credit);

    }




}
