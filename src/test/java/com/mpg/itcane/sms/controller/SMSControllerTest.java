package com.mpg.itcane.sms.controller;


import com.mpg.itcane.sms.entity.Parameter;
import com.mpg.itcane.sms.entity.ParameterDetail;
import com.mpg.itcane.sms.repository.ParameterDetailRepository;
import com.mpg.itcane.sms.repository.ParameterRepository;
import com.mpg.itcane.sms.util.CommonUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class SMSControllerTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    ParameterRepository parameterRepository;

    @Autowired
    ParameterDetailRepository parameterDetailRepository;


    @Before
    public void initData(){
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void caseSendSMS_WithoutPhoneNumber_should_be_success() throws Exception {
        mockMvc.perform(post("/sms/sendSms/simple")
                .param("message", " TEST : caseSendSMS_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp())

                )
                .andExpect(content().json(
                        "{\"success\": false,\"message\": \"Mobile number has require to send.\",\"data\": {},\"error_code\":\"SMSERROR001\"}"));

    }

    @Test
    public void caseSendSMS_NoMessage_should_be_success() throws Exception {
        mockMvc.perform(post("/sms/sendSms/simple")
                .param("to", "0618423814")

        )
                .andExpect(status().isOk()).andExpect(content().json(
                "{\"success\": true,\"message\": \"Send sms success.\",\"data\": {} }"));

    }

    @Test
    public void caseSendSMS_NoTemplate_should_be_fail() throws Exception {
        mockMvc.perform(post("/sms/sendSms/xxx")
                .param("to", "0618423814")
                .param("message", " TEST : caseSendSMS_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp())

        )
                .andExpect(content().json(
                        "{\"success\": false,\"message\": \"Not found sms template.\",\"data\": {},\"error_code\":\"SMSERROR002\"}"));

    }

    @Test
    public void caseSendSMS_HaveSpecialCharacter_should_be_fail() throws Exception {
        mockMvc.perform(post("/sms/sendSms/simple")
                .param("to", "0618423814")
                .param("message", " #### TEST : caseSendSMS_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp())

        )
                .andExpect(content().json(
                        "{\"success\": false,\"message\": \"Parameter not complete\",\"data\": {},\"error_code\":\"SMSERR101\"}"));

    }

    @Test
    public void caseSendSMS_InvalidNumber_should_be_fail() throws Exception {
        mockMvc.perform(post("/sms/sendSms/simple")
                .param("to", "061842381412345")
                .param("message", " TEST : caseSendSMS_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp())

        )
                .andExpect(content().json(
                        "{\"success\": false,\"message\": \"Invalid Mobile format.\",\"data\": {},\"error_code\":\"SMSERR104\"}"));

    }

    @Test
    public void caseSendSMSByProvider_should_be_success() throws Exception {
        mockMvc.perform(post("/sms/sendSmsByProviderCode/SMSMKT/simple")
                .param("code", "MKT001")
                .param("to", "0618423814")
                .param("message", " TEST : caseSendSMS_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp())

        )
                .andExpect(status().isOk()).andExpect(content().json(
                "{\"success\": true,\"message\": \"Send sms success.\",\"data\": {} }"));

    }

    @Test
    public void caseSendSMSByProvider_WithoutPhoneNumber_should_be_success() throws Exception {
        mockMvc.perform(post("/sms/sendSmsByProviderCode/SMSMKT/simple")
                .param("code", "MKT001")
                .param("message", " TEST : caseSendSMS_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp())
        )
                .andExpect(content().json(
                        "{\"success\": false,\"message\": \"Mobile number has require to send.\",\"data\": {},\"error_code\":\"SMSERROR001\"}"));

    }

    @Test
    public void caseSendSMSByProvider_NoTemplate_should_be_fail() throws Exception {
        mockMvc.perform(post("/sms/sendSmsByProviderCode/SMSMKT")
                .param("code", "MKT001")
                .param("to", "0618423814")
                .param("message", " TEST : caseSendSMS_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp())

        )
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void caseSendSMSByProvider_NoProvider_should_be_fail() throws Exception {
        mockMvc.perform(post("/sms/sendSmsByProviderCode/simple")
                .param("code", "MKT001")
                .param("to", "0618423814")
                .param("message", " TEST : caseSendSMS_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp())

        )
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void caseSendSMSByProvider_NoMessage_should_be_success() throws Exception {
        mockMvc.perform(post("/sms/sendSmsByProviderCode/SMSMKT/simple")
                .param("code", "MKT001")
                .param("to", "0618423814")

        )
                .andExpect(status().isOk()).andExpect(content().json(
                "{\"success\": true,\"message\": \"Send sms success.\",\"data\": {} }"));

    }

    @Test
    public void caseSendSMSByProvider_InvalidNumber_should_be_fail() throws Exception {
        mockMvc.perform(post("/sms/sendSmsByProviderCode/SMSMKT/simple")
                .param("code", "MKT001")
                .param("to", "1111111111111111")
                .param("message", " TEST : caseSendSMS_should_be_success Send Date : "+CommonUtil.getCurrentDateTimestamp())

        )
                .andExpect(content().json(
                        "{\"success\": false,\"message\": \"Invalid Mobile format.\",\"data\": {},\"error_code\":\"SMSERR104\"}"));

    }

}
