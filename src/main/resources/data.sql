insert into parameter(id,code,description,flag_active) values(3000,'SMS001','กำหนดค่า Configure SMS Gateway',1);
insert into parameter(id,code,description,flag_active) values(4000,'SMS002','กำหนด Template SMS Message',1);

insert into parameter_detail(id,name,code,create_date,variable1,variable2,variable3,variable4,variable5,variable6 ,flag_active,parameter)
values(3001,'SMSMKT.COM','MKT001','2019-10-09 00:00:00','SMSMKT','https://member.smsmkt.com/SMSLink/SendMsg/index.php','Mitrphol@SMS','Az4Y5Bxk',null,'Y',1,3000);
insert into parameter_detail(id,name,code,create_date,variable1,variable2,variable3,variable4 ,variable6,flag_active,parameter)
values(3002,'SMSMKT.COM','MKT002','2019-10-09 00:00:00','SMSMKT','https://member.smsmkt.com/SMSLink/GetCredit/index.php','Mitrphol@SMS','Az4Y5Bxk','Y',1,3000);
insert into parameter_detail(id,code,create_date,variable1 ,flag_active,parameter)
values(4001,'simple','2019-10-09 00:00:00','....simple template... msg : $message',1,4000);