package com.mpg.itcane.sms;

import com.mpg.itcane.sms.builder.ClickNextPatternBuilder;
import com.mpg.itcane.sms.builder.SMSPatternBuilder;

public final  class SMSPatternFactory {

    private SMSPatternFactory(){

    }

    public static SMSPatternBuilder builder(String providerCode){
        SMSPatternBuilder smsPatternBuilder = null;
        switch (providerCode){
            case ApplicationConstant.PROVIDER_CLICK_NEXT :
                smsPatternBuilder = new ClickNextPatternBuilder();
                break;
            default:
                /* default is click next provider*/
                smsPatternBuilder = new ClickNextPatternBuilder();
                break;
        }
        return  smsPatternBuilder;
    }

}
