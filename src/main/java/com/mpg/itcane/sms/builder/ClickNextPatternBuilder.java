package com.mpg.itcane.sms.builder;

import com.mpg.itcane.sms.ApplicationConstant;
import com.mpg.itcane.sms.exception.SMSException;
import com.mpg.itcane.sms.model.ResponseModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class ClickNextPatternBuilder extends SMSPatternBuilder {


    @Override
    public ResponseModel sendSMS(Map mapConfig, ResponseModel responseModel) {
        log.info("========= mapConfig send SMS [{}]",mapConfig);
        StringBuilder sb = new StringBuilder();

        ResponseEntity<String> reponseEntity =null;
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/x-www-form-urlencoded");

        /* append parameter to send */
        sb.append("?User=");
        sb.append(mapConfig.get("user"));
        sb.append("&Password=");
        sb.append(mapConfig.get("pass"));
        sb.append("&Msnlist=");
        sb.append(mapConfig.get(ApplicationConstant.KEY_MOBILE_LIST));
        sb.append("&Msg=");
        sb.append(mapConfig.get("message"));
        sb.append("&Sender=");
        sb.append(mapConfig.get("name"));

        String url = mapConfig.get("url")+sb.toString();
        HttpEntity<Object> entity = new HttpEntity<>("", headers);
        reponseEntity = restTemplate.exchange(url, HttpMethod.GET,entity, String.class);

        responseModel.setMessage(reponseEntity.getBody());
        return responseModel;
    }

    @Override
    public ResponseModel checkCredit(Map mapConfig, ResponseModel responseModel) {
        StringBuilder sb = new StringBuilder();
        ResponseEntity<String> reponseEntity =null;
        String credit="";
        try{

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("Content-Type", "application/x-www-form-urlencoded");

            sb.append("?User=");
            sb.append(mapConfig.get("user"));
            sb.append("&Password=");
            sb.append(mapConfig.get("pass"));
            String url = String.valueOf(mapConfig.get("url")) + sb.toString();

            HttpEntity<Object> entity = new HttpEntity<>("", headers);
            reponseEntity = restTemplate.exchange(url,HttpMethod.GET,entity, String.class);

            Map<String, String> responseCredit = seperateResponseCheckCredit(reponseEntity.getBody());
            credit = String.valueOf(responseCredit.get(ApplicationConstant.KEY_BALANCE));
            log.info("response credit balance amount = {}",credit);
            responseModel.setData(Integer.parseInt(credit));
            return responseModel;

        }catch(Exception e){
            log.error("checkCredit:Exception:{}",e.getMessage());
            throw new SMSException("Can't check credit sms Msg :"+e.getMessage());

        }
    }

    @Override
    public ResponseModel response(String responseText, ResponseModel responseModel) {
        responseText = (responseText.split(",")[0].split("=")[1]);
        switch (responseText){
            case "-101":
                responseModel.setError_code(ApplicationConstant.RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.get("-101"));
                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_FAIL);
                responseModel.setMessage(ApplicationConstant.RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.get("-101"));
                break;
            case "-102":
                responseModel.setError_code(ApplicationConstant.RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.get("-102"));
                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_FAIL);
                responseModel.setMessage(ApplicationConstant.RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.get("-102"));
                break;
            case "-103":
                responseModel.setError_code(ApplicationConstant.RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.get("-103"));
                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_FAIL);
                responseModel.setMessage(ApplicationConstant.RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.get("-103"));
                break;
            case "-104":
                responseModel.setError_code(ApplicationConstant.RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.get("-104"));
                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_FAIL);
                responseModel.setMessage(ApplicationConstant.RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.get("-104"));
                break;
            case "-105":
                responseModel.setError_code(ApplicationConstant.RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.get("-105"));
                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_FAIL);
                responseModel.setMessage(ApplicationConstant.RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.get("-105"));
                break;
            case "-106":
                responseModel.setError_code(ApplicationConstant.RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.get("-106"));
                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_FAIL);
                responseModel.setMessage(ApplicationConstant.RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.get("-106"));
                break;
            case "-107":
                responseModel.setError_code(ApplicationConstant.RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.get("-107"));
                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_FAIL);
                responseModel.setMessage(ApplicationConstant.RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.get("-107"));
                break;
            case "-108":
                responseModel.setError_code(ApplicationConstant.RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.get("-108"));
                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_FAIL);
                responseModel.setMessage(ApplicationConstant.RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.get("-108"));
                break;
            case "-109":
                responseModel.setError_code(ApplicationConstant.RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.get("-109"));
                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_FAIL);
                responseModel.setMessage(ApplicationConstant.RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.get("-109"));
                break;
            case "-110":
                responseModel.setError_code(ApplicationConstant.RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.get("-110"));
                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_FAIL);
                responseModel.setMessage(ApplicationConstant.RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.get("-110"));
                break;
            case "-111":
                responseModel.setError_code(ApplicationConstant.RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.get("-111"));
                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_FAIL);
                responseModel.setMessage(ApplicationConstant.RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.get("-111"));
                break;
            case "-112":
                responseModel.setError_code(ApplicationConstant.RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.get("-112"));
                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_FAIL);
                responseModel.setMessage(ApplicationConstant.RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.get("-112"));
                break;
            default:
                responseModel.setError_code(null);
                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_SUCCESS);
                responseModel.setMessage("Send sms success.");
                break;
        }
        return responseModel;
    }

    public Map<String, String> seperateResponseCheckCredit(String str){

        /*      Status=0,Detail=Your account have 66 SMS@Valid thru 31 December 2562@Within 84 Days      */
        Map<String, String> responseMap = new HashMap<>();
        if(str.contains("Status=0")){
            String[] arrResponse = str.split(" ");
            responseMap.put(ApplicationConstant.KEY_BALANCE,(arrResponse[3]).toString().replaceAll(",",""));
            responseMap.put("detail","");
        }else{
            String[] arrResponse = str.split(",");
            responseMap.put(ApplicationConstant.KEY_BALANCE,"0");
            responseMap.put("detail",arrResponse[1]);
        }

        return responseMap;
    }
}
