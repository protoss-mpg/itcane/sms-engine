package com.mpg.itcane.sms.builder;

import com.mpg.itcane.sms.model.ResponseModel;

import java.util.Map;

public abstract class SMSPatternBuilder {

   public abstract ResponseModel sendSMS(Map mapConfig, ResponseModel responseModel);
   public  abstract ResponseModel checkCredit(Map mapConfig, ResponseModel responseModel);
   public  abstract ResponseModel response(String responseText, ResponseModel responseModel);

}
