package com.mpg.itcane.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.mpg.itcane.sms.entity.ParameterDetail;

public interface ParameterDetailRepository extends JpaSpecificationExecutor<ParameterDetail>,
		JpaRepository<ParameterDetail, Long>, PagingAndSortingRepository<ParameterDetail, Long> {

	@Query("select DISTINCT pd from ParameterDetail pd " + "left join pd.parameter p "
			+ "where p.code in :parameterCode " + "and pd.code = :code " + "and pd.flagActive = true "
			+ "order by pd.id ")
	List<ParameterDetail> findListDetailByParameterCodeAndDetailCodeAndFlagActiveIsActive(
			@Param("parameterCode") List<String> parameterCode, @Param("code") String code);

	@Query("select DISTINCT pd from ParameterDetail pd " + "left join pd.parameter p "
			+ "where p.code in :parameterCode "
			+ "and pd.variable6 = 'Y' "
			+ "and pd.flagActive = true "
			+ "order by pd.id ")
	List<ParameterDetail> findListDetailByParameterCodeAndDetailCodeAndVariable6IsYAndFlagActiveIsActive(@Param("parameterCode") List<String> parameterCode);




}
