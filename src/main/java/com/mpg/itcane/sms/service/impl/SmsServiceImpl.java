package com.mpg.itcane.sms.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.StringReader;
import java.io.StringWriter;

import com.mpg.itcane.sms.ApplicationConstant;
import com.mpg.itcane.sms.SMSPatternFactory;
import com.mpg.itcane.sms.builder.SMSPatternBuilder;
import com.mpg.itcane.sms.entity.ParameterDetail;
import com.mpg.itcane.sms.exception.SMSException;
import com.mpg.itcane.sms.model.ResponseModel;
import com.mpg.itcane.sms.repository.ParameterDetailRepository;
import com.mpg.itcane.sms.service.SmsService;

import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.runtime.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.node.SimpleNode;

@Slf4j
@Service
public class SmsServiceImpl  implements SmsService {

    @Autowired
    ParameterDetailRepository parameterDetailRepository;



    @Override
    public ResponseModel sendSms(Map mapParam, ResponseModel responseModel, String templateCode) {
        try {
            SMSPatternBuilder smsPatternBuilder =  SMSPatternFactory.builder("");
            String messageSend = "";
            List<String> listParamConfigSMS = new ArrayList<>();
            listParamConfigSMS.add(ApplicationConstant.PARAMETER_CONFIG_SMS);
            /* find sms properties casee default */
            List<ParameterDetail> listParamDetail = parameterDetailRepository.findListDetailByParameterCodeAndDetailCodeAndVariable6IsYAndFlagActiveIsActive(
                    listParamConfigSMS
            );

            if(listParamDetail.isEmpty()){
                throw new SMSException("Not found config for sms parameter code : "+ApplicationConstant.PARAMETER_CONFIG_SMS);
            }else{
                ParameterDetail paramConfig = listParamDetail.get(0);

                mapParam.put("url",paramConfig.getVariable2());
                mapParam.put("user",paramConfig.getVariable3());
                mapParam.put("pass",paramConfig.getVariable4());
                mapParam.put("name",paramConfig.getName());


                /* check mobile number*/
                if(mapParam.get("to") == null || "".equals(mapParam.get("to"))){
                    responseModel.setError_code(ApplicationConstant.ERROR_CODE_DATA_HAS_REQUIRE);
                    throw new SMSException("Mobile number has require to send.");
                }
                if(null == mapParam.get("to") || "".equals(mapParam.get("to")))
                    mapParam.put(ApplicationConstant.KEY_MOBILE_LIST,paramConfig.getVariable5());
                else
                    mapParam.put(ApplicationConstant.KEY_MOBILE_LIST,mapParam.get("to"));

                /* find template message with template code */
                listParamConfigSMS.clear();
                listParamConfigSMS.add(ApplicationConstant.PARAMETER_CONFIG_TEMPLATE_SMS);
                List<ParameterDetail> listTemplateLineMessage = parameterDetailRepository
                        .findListDetailByParameterCodeAndDetailCodeAndFlagActiveIsActive(
                                listParamConfigSMS,
                                templateCode);

                if (!listTemplateLineMessage.isEmpty()) {
                    String templateMessage = listTemplateLineMessage.get(0).getVariable1();
                    /* pass parameter to template message */
                    String templateFillter = getContentFromTemplate(templateMessage, mapParam);
                        messageSend = templateFillter;
                        mapParam.put("message",messageSend);

                        /* send sms */
                        smsPatternBuilder.sendSMS(mapParam,responseModel);
                        String responseMsg = responseModel.getMessage();

                        if(null != responseMsg){
                            smsPatternBuilder.response(responseMsg,responseModel);
                        }
                        return responseModel;

                }else{
                    responseModel.setError_code(ApplicationConstant.ERROR_CODE_NOT_FOUND_TEMPLATE);
                    throw new SMSException("Not found sms template.");
                }
            }
        } catch (Exception e) {
            /* sent object response for exception case */
            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_FAIL);
            responseModel.setMessage(e.getMessage());
            log.error(e.getMessage());
            return responseModel;

        }
    }

    @Override
    public ResponseModel sendSmsByProviderCode(Map mapParam, ResponseModel responseModel,String provider, String templateCode) {
        try{
          SMSPatternBuilder smsPatternBuilder =  SMSPatternFactory.builder(provider);

            String messageSend = "";
            List<String> listParamConfigSMS = new ArrayList<>();
            listParamConfigSMS.add(ApplicationConstant.PARAMETER_CONFIG_SMS);
            List<ParameterDetail> listParamDetail = parameterDetailRepository.findListDetailByParameterCodeAndDetailCodeAndFlagActiveIsActive(
                    listParamConfigSMS,String.valueOf(mapParam.get("code"))
            );

            if(listParamDetail.isEmpty()){
                throw new SMSException("Not found parameter config for sms in provider code : "+(mapParam.get("code")));
            }else{
                ParameterDetail paramConfig = listParamDetail.get(0);

                mapParam.put("url",paramConfig.getVariable2());
                mapParam.put("user",paramConfig.getVariable3());
                mapParam.put("pass",paramConfig.getVariable4());
                mapParam.put("name",paramConfig.getName());

                /* check mobile number*/
                if(mapParam.get("to") == null || "".equals(mapParam.get("to"))){
                    responseModel.setError_code(ApplicationConstant.ERROR_CODE_DATA_HAS_REQUIRE);
                    throw new SMSException("Mobile number has require to send.");
                }
                if(null == paramConfig.getVariable5() || "".equals(paramConfig.getVariable5()))
                    mapParam.put(ApplicationConstant.KEY_MOBILE_LIST,mapParam.get("to"));
                else
                    mapParam.put(ApplicationConstant.KEY_MOBILE_LIST,paramConfig.getVariable5());

                /* find template message with template code */
                listParamConfigSMS.clear();
                listParamConfigSMS.add(ApplicationConstant.PARAMETER_CONFIG_TEMPLATE_SMS);
                List<ParameterDetail> listTemplateLineMessage = parameterDetailRepository
                        .findListDetailByParameterCodeAndDetailCodeAndFlagActiveIsActive(
                                listParamConfigSMS,
                                templateCode);

                if (!listTemplateLineMessage.isEmpty()) {
                    String templateMessage = listTemplateLineMessage.get(0).getVariable1();
                    /* pass parameter to template message */
                    String templateFillter = getContentFromTemplate(templateMessage, mapParam);

                        messageSend = templateFillter;
                        mapParam.put("message",messageSend);

                        /* send sms */
                    log.info("after send sms map {}",mapParam);
                        smsPatternBuilder.sendSMS(mapParam,responseModel);

                    String responseMsg = responseModel.getMessage();

                    if(null != responseMsg){
                        smsPatternBuilder.response(responseMsg,responseModel);
                    }
                    return responseModel;

                }else{
                    responseModel.setError_code(ApplicationConstant.ERROR_CODE_NOT_FOUND_TEMPLATE);
                    throw new SMSException("Not found sms template.");
                }
            }

        }catch (Exception e){
            responseModel.setMessage(e.getMessage());
            return responseModel;
        }

    }

    @Override
    public ResponseModel checkCreditBalance(ResponseModel responseModel) {
        try{
            SMSPatternBuilder smsPatternBuilder =  SMSPatternFactory.builder("");
            Map mapParam = new HashMap();
            List<String> listParamConfigSMS = new ArrayList<>();
            listParamConfigSMS.add(ApplicationConstant.PARAMETER_CONFIG_SMS);
            List<ParameterDetail> listParamDetail = parameterDetailRepository.findListDetailByParameterCodeAndDetailCodeAndFlagActiveIsActive(
                    listParamConfigSMS,ApplicationConstant.SMS_MKT_CHECK_CREDIT
            );

            if(listParamDetail.isEmpty()){
                throw new SMSException("Not found parameter config for sms.");
            }else{
                ParameterDetail paramConfig = listParamDetail.get(0);

                mapParam.put("url",paramConfig.getVariable2());
                mapParam.put("user",paramConfig.getVariable3());
                mapParam.put("pass",paramConfig.getVariable4());

                smsPatternBuilder.checkCredit(mapParam,responseModel);
                int result = Integer.parseInt(String.valueOf(responseModel.getData())) ;

                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_SUCCESS);
                responseModel.setError_code(null);
                responseModel.setData(result);
                responseModel.setMessage("Check sms credit balance.");
                return responseModel;
            }

        }catch (Exception e){
            responseModel.setMessage(e.getMessage());
            responseModel.setError_code(ApplicationConstant.ERROR_CODE_NOT_FOUND_TEMPLATE);
            return responseModel;
        }


    }

    private String getContentFromTemplate(String templateContent, Map mapContext) {

        try{
            RuntimeServices runtimeServices;
            StringReader stringReader;
            SimpleNode simpleNode;
            Template template;

            if (templateContent != null && templateContent.trim().length() > 0) {
                runtimeServices = RuntimeSingleton.getRuntimeServices();
                stringReader = new StringReader(templateContent);
                simpleNode = runtimeServices.parse(stringReader, "SMS Template");

                template = new Template();
                template.setRuntimeServices(runtimeServices);
                template.setData(simpleNode);
                template.initDocument();

                /* add that list to a VelocityContext */
                VelocityContext context = new VelocityContext(mapContext);

                /* get the Template */
                /* now render the template into a Writer */
                StringWriter writer = new StringWriter();
                template.merge(context, writer);
                return writer.toString();
            } else {
                return templateContent;
            }

        }catch (ParseException e){
            log.error("getContentFromTemplateParseException : {}",e.getMessage());
            return templateContent;
        }
        catch (SMSException e){
            log.error("getContentFromTemplateSMSException : {}",e.getMessage());
            return templateContent;
        }


    }

}