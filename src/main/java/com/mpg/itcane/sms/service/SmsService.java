package com.mpg.itcane.sms.service;

import java.util.Map;

import com.mpg.itcane.sms.model.ResponseModel;

public interface SmsService {

    ResponseModel sendSms(Map mapParam,ResponseModel result, String templateCode);
    ResponseModel sendSmsByProviderCode(Map mapParam, ResponseModel  result, String provider, String templateCode);
    ResponseModel checkCreditBalance(ResponseModel responseModel);
}