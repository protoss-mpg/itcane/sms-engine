package com.mpg.itcane.sms.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.mpg.itcane.sms.model.ResponseModel;
import com.mpg.itcane.sms.service.SmsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/sms")
@Slf4j
@Api(description = "Service Of SMS Engine")
public class SmsController{

    @Autowired
    SmsService smsService;

    @ApiOperation("Example for parameter(form-urlencoded) : [ to=0812345678, message=send_by_smsEngine ]  ")
    @PostMapping("/sendSms/{templateCode}")
    public ResponseEntity<ResponseModel> sendSms(HttpServletRequest request,@PathVariable String templateCode) {
         /*object response */
         ResponseModel result = new ResponseModel();
         HttpHeaders headers = new HttpHeaders();
         headers.add("Content-Type", "application/json; charset=utf-8");

         try {
              /*read request parameterMap to map */
              Map<String, String> mapContext = new HashMap<>();
              Map<String, String[]> paramerers = request.getParameterMap();
             for(Map.Entry<String, String[]> entryKey:paramerers.entrySet()){
                 String key = entryKey.getKey();
                 String value = paramerers.get(key)[0];
                   mapContext.put(String.valueOf(key), value);
              }

              result = smsService.sendSms(mapContext, result,templateCode);
              return ResponseEntity.ok().headers(headers).body(result);

         } catch (Exception e) {
             log.error(e.getMessage());
              return ResponseEntity.ok().headers(headers).body(result);

         }

    }

    @ApiOperation("Example for parameter(form-urlencoded) : [ code=SMSMKT, to=0812345678, message=send_by_smsEngine ]  ")
    @PostMapping("/sendSmsByProviderCode/{provider}/{templateCode}")
    public ResponseEntity<ResponseModel> sendSmsByProviderCode(HttpServletRequest request,@PathVariable("provider")String provider,@PathVariable("templateCode") String templateCode) {
         /*object response */
         ResponseModel result = new ResponseModel();
         HttpHeaders headers = new HttpHeaders();
         headers.add("Content-Type", "application/json; charset=utf-8");

         try {
              /*read request parameterMap to map */
              Map<String, String> mapContext = new HashMap<>();
              Map<String, String[]> paramerers = request.getParameterMap();
             for(Map.Entry<String, String[]> entryKey:paramerers.entrySet()){
                 String key = entryKey.getKey();
                 String value = paramerers.get(key)[0];
                   mapContext.put(String.valueOf(key), value);
              }
              
              result = smsService.sendSmsByProviderCode(mapContext, result,provider,templateCode);
              return ResponseEntity.ok().headers(headers).body(result);

         } catch (Exception e) {
             log.error(e.getMessage());
              return ResponseEntity.ok().headers(headers).body(result);

         }

    }




}