package com.mpg.itcane.sms;

import java.util.HashMap;
import java.util.Map;

public final class ApplicationConstant {



	private ApplicationConstant(){

		}

	public static final boolean RESPONSE_STATUS_SUCCESS = true;
	public static final boolean RESPONSE_STATUS_FAIL = false;

	public static final String ERROR_CODE_PROCESS_FAIL        = "SMSERR000";
	public static final String ERROR_CODE_DATA_HAS_REQUIRE = "SMSERROR001";
	public static final String ERROR_CODE_NOT_FOUND_TEMPLATE = "SMSERROR002";
	public static final String KEY_BALANCE = "BALANCE";
	public static final String KEY_MOBILE_LIST = "MOBILE_LIST";

	public static final String PARAMETER_CONFIG_SMS = "SMS001";
	public static final String PARAMETER_CONFIG_TEMPLATE_SMS = "SMS002";


	public static final String SMS_MKT_CHECK_CREDIT = "MKT002";
	public static final String PROVIDER_CLICK_NEXT = "SMSMKT";


	public static final Map<String,String> RESPONSE_STATUS_MESSAGE_SMSMKT_MAP = new HashMap<>();
	static {
		RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.put("-101","Parameter not complete");
		RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.put("-102","SMS database is not ready.");
		RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.put("-103","Invalid User / Invalid Password.");
		RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.put("-104","Invalid Mobile format.");
		RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.put("-105","Mobile number length limit exceed.");
		RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.put("-106","Invalid sms send name.");
		RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.put("-107","Accout is expired.");
		RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.put("-108","Quota limit exceed.");
		RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.put("-109","SMS system is not ready. try again");
		RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.put("-110","SMS account has been locked.");
		RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.put("-111","Invalid Message found special characters.");
		RESPONSE_STATUS_MESSAGE_SMSMKT_MAP.put("-112","Moblie number blacklisted.");
	}


	public static final Map<String,String> RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP = new HashMap<>();
	static {
		RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.put("-101","SMSERR101");
		RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.put("-102","SMSERR102");
		RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.put("-103","SMSERR103");
		RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.put("-104","SMSERR104");
		RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.put("-105","SMSERR105");
		RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.put("-106","SMSERR106");
		RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.put("-107","SMSERR107");
		RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.put("-108","SMSERR108");
		RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.put("-109","SMSERR109");
		RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.put("-110","SMSERR110");
		RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.put("-111","SMSERR111");
		RESPONSE_STATUS_CODE_ERROR_SMSMKT_MAP.put("-112","SMSERR112");
	}


}
