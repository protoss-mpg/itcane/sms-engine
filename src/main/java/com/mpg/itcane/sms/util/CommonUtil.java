package com.mpg.itcane.sms.util;

import com.mpg.itcane.sms.exception.SMSException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class CommonUtil {

    private  CommonUtil(){

    }

	public static Timestamp getCurrentDateTimestamp() {
        Timestamp today = null;
        try {
            Date nowDate = Calendar.getInstance().getTime();
            today = new java.sql.Timestamp(nowDate.getTime());
        } catch (Exception e) {
            throw new SMSException("Cant get current date",e);
        }
        return today;
    }


}
