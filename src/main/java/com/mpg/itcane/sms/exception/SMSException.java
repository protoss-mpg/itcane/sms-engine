package com.mpg.itcane.sms.exception;

public class SMSException extends RuntimeException {

    public SMSException(){

    }
    public SMSException(String errorMessage, Throwable err) {
        super(errorMessage, err);

    }
    public SMSException(String errorMessage) {
        super(errorMessage);

    }
}