package com.mpg.itcane.sms.configuration.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;


import com.mpg.itcane.sms.util.CommonUtil;

@Slf4j
@Component
@Aspect
public class LoggingAspect {


    @Pointcut("@annotation(com.mpg.itcane.sms.configuration.aop.annotation.LogAccess)")
    public void logAccessMethod() {
        /* this log will work when add annotation in head method */
    }

    @Around("logAccessMethod()")
    public Object profile(ProceedingJoinPoint pjp) throws Throwable {
            long start = System.currentTimeMillis();
            Object output = pjp.proceed();
            long elapsedTime = System.currentTimeMillis() - start;
            log.info(pjp.getSignature().getName()+"|"+CommonUtil.getCurrentDateTimestamp()+"|"+elapsedTime);
           
            return output;
    }

}

